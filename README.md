![logo](resources/images/ridgeline.jpg)


# Efficiency Maine Trust - Residential Heat Pump Study

## Background

From Efficiency Maine [website](https://www.efficiencymaine.com/about/):

> The Trust is the independent administrator for programs to improve the
> efficiency of energy use and reduce greenhouse gases in Maine. The Trust does
> this primarily by delivering financial incentives on the purchase of
> high-efficiency equipment or changes to operations that help customers save
> electricity, natural gas and other fuels throughout the Maine economy. The
> Trust is a quasi-state agency governed by a Board of Trustees with oversight
> from the Maine Public Utilities Commission.

## Project management

### TODOs

- 

### Scope

- We will complete an independent impact evaluation of residential heat pump
    measures under the Home Energy Savings Program and Affordable Heat
    Initiative covering the period from July 1, 2019 through June 30, 2021.
    The study will quantify and verify electric energy and demand impacts,
    verify non-electric energy impacts, and analyze program cost-effectiveness.
    Consistent with these objectives, the overall evaluation project will
    include an impact evaluation and a cost-benefit analysis.

### Contacts

#### Ridgeline Analytics
| Staff | Role | Email |
| ----- | ---- | ----- |
| David Korn | VP | dkorn@ridgelineanalytics.com |
| Ari Jackson | Senior Associate | ajackson@ridgelineanalytics.com |
| Will Rambur | Engineer | wrambur@ridgelineanalytics.com |

#### Efficiency Maine
| Staff | Role | Email |
| ----- | ---- | ----- |
| Laura Martel | Research and Evaluation Manager | laura.martel@efficiencymaine.com |

## Data

### Sources

- [ThingSpeak](https://thingspeak.com/):
    Time series data storage and API access
    
- [Particle](https://www.particle.io/):
    Cellular device monitoring
    
- [Fulcrum](https://www.fulcrumapp.com/):
    Field data collection, CSV files exported from website
    
- [SentientThings](https://ridgeline.sentientthings.com/):
    Data QC and notification system

## Source Code

- [GitLab](https://gitlab.com/arijackson/emt_dhp)

### Scripts

- write_env.py: create .env file for project using root for project directory

- write_dirs.py: create all directories needed to run pipeline

- preprocess1.py: cleaning/processing fulcrum data

- preprocess2.py: download timeseries data from thingspeak

- process1.py: merging fulcrum data

- process2.py: process and clean thingspeak data

- util.py: processing data based on user input

### Modules

- particle_helper: useful functions for pulling data from particle API

- thingspeak_helper: useful functions for pulling data from thingspeak API

- qc_plot: plots for qc of timeseries data
