# -*- coding: utf-8 -*-
"""
Script to write all directorys needed to excute remaining scripts in src
"""
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')


# \root
sup_dir_path = root
dir_list = ['data']
for dir_name in dir_list:
    dir_path = sup_dir_path + '/' + dir_name
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
# \data
sup_dir_path = root + '/data'
dir_list = ['raw', 'interim', 'processed']
for dir_name in dir_list:
    dir_path = sup_dir_path + '/' + dir_name
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
# \data\raw
sup_dir_path = root + '/data/raw'
dir_list = ['project', 'fulcrum', 'particle', 'sd', 'thingspeak']
for dir_name in dir_list:
    dir_path = sup_dir_path + '/' + dir_name
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
# \data\raw\fulcrum
sup_dir_path = root + '/data/raw/fulcrum'
dir_list = ['images']
for dir_name in dir_list:
    dir_path = sup_dir_path + '/' + dir_name
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
# \data\interim
sup_dir_path = root + '/data/interim'
dir_list = ['project', 'fulcrum', 'thingspeak', 'sd']
for dir_name in dir_list:
    dir_path = sup_dir_path + '/' + dir_name
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
# \data\interim
sup_dir_path = root + '/data/interim/thingspeak'
dir_list = ['compressor_power', 'fan_current', 'outdoor_temp',
            'supply_temp', 'supply_rh', 'return_temp',
            'outdoor_battery', 'indoor_battery']
for dir_name in dir_list:
    dir_path = sup_dir_path + '/' + dir_name
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
# \data\processed
sup_dir_path = root + '/data/processed'
dir_list = ['fulcrum', 'thingspeak', 'plots']
for dir_name in dir_list:
    dir_path = sup_dir_path + '/' + dir_name
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
# \data\processed\plots
sup_dir_path = root + '/data/processed/plots'
dir_list = ['sites']
for dir_name in dir_list:
    dir_path = sup_dir_path + '/' + dir_name
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
