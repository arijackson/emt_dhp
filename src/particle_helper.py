# -*- coding: utf-8 -*-
"""
some useful functions for pulling meta data from particle API
"""
import pandas as pd
import requests
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
particle_token = os.environ.get('PT_TK')


def particle_devices():
    """
    """
    cols = [
        'device_id', 'device_name',
        'last_heard', 'last_handshake',
        'online', 'connected', 'status',
        'serial_number', 'iccid', 'last_iccid', 'imei',
        'system_firmware_version', 'current_build_target',
        'default_build_target']
    df = pd.DataFrame(columns=cols)
    base_url = 'https://api.particle.io/v1'
    end_point = 'devices'
    request_url = base_url + '/' + end_point
    r = requests.get(
        request_url,
        headers={'Authorization': 'Bearer ' + particle_token})
    device_list = r.json()
    for device in device_list:
        device_id = device['id']
        device_name = device['name']
        try:
            last_heard = device['last_heard']
        except KeyError:
            last_heard = ''
        try:
            last_handshake = device['last_handshake_at']
        except KeyError:
            last_handshake = ''
        online = device['online']
        connected = device['connected']
        try:
            status = device['status']
        except KeyError:
            status = ''
        serial_number = device['serial_number']
        try:
            iccid = device['iccid']
        except KeyError:
            iccid = ''
        try:
            last_iccid = device['last_iccid']
        except KeyError:
            last_iccid = ''
        try:
            imei = device['imei']
        except KeyError:
            imei = ''
        try:
            system_firmware_version = device['system_firmware_version']
        except KeyError:
            system_firmware_version = ''
        try:
            current_build_target = device['current_build_target']
        except KeyError:
            current_build_target = ''
        try:
            default_build_target = device['default_build_target']
        except KeyError:
            default_build_target = ''
        vals = [
            device_id, device_name,
            last_heard, last_handshake,
            online, connected, status,
            serial_number, iccid, last_iccid, imei,
            system_firmware_version, current_build_target,
            default_build_target]
        df = df.append(pd.DataFrame(columns=cols, data=[vals]))
    df['last_heard'] = pd.to_datetime(df['last_heard'])
    df['last_handshake'] = pd.to_datetime(df['last_handshake'])
    df['last_heard'] = df['last_heard'].dt.tz_convert('America/New_York')
    df['last_handshake'] = df['last_handshake'].dt.tz_convert(
        'America/New_York')
    df['last_heard'] = df['last_heard'].dt.strftime(
        '%m/%d/%Y %H:%M')
    df['last_handshake'] = df['last_handshake'].dt.strftime(
        '%m/%d/%Y %H:%M')
    df.to_csv(
        root + '/data/raw/particle/devices.csv', index=False)


def device_vitals():
    """
    """
    df = pd.read_csv(
        root + '/data/raw/particle/devices.csv', dtype=str)
    device_list = df['device_id'].tolist()
    cols = [
        'device_id',
        'signal_strength', 'signal_quality',
        'connection_status', 'connection_attempts',
        'connection_disconnects', 'connection_disconnection_reason',
        'power_source', 'power_charge', 'power_state', 'system_uptime',
        'memory_used', 'memory_total', 'memory_pct_used', 'device_status']
    df = pd.DataFrame(columns=cols)
    base_url = 'https://api.particle.io/v1'
    for device_id in device_list:
        end_point = 'diagnostics/' + device_id + '/last'
        request_url = base_url + '/' + end_point
        r = requests.get(
            request_url,
            headers={'Authorization': 'Bearer ' + particle_token})
        vitals = r.json()
        if vitals == dict():
            vals = [
                device_id,
                '', '', '', '', '', '', '', '', '', '', '', '', '', '']
            df = df.append(pd.DataFrame(columns=cols, data=[vals]))
            continue
        signal_strength = (
                vitals['diagnostics']['payload']['device'][
                    'network']['signal']['strength'])
        signal_quality = (
                vitals['diagnostics']['payload']['device'][
                    'network']['signal']['quality'])
        connection_status = (
             vitals['diagnostics']['payload']['device'][
                'cloud']['connection']['status'])
        connection_attempts = (
            vitals['diagnostics']['payload']['device'][
                'cloud']['connection']['attempts'])
        connection_disconnects = (
            vitals['diagnostics']['payload']['device'][
                'cloud']['connection']['disconnects'])
        connection_disconnection_reason = (
            vitals['diagnostics']['payload']['device'][
                'cloud']['connection']['disconnect_reason'])
        power_source = (
            vitals['diagnostics']['payload']['device'][
                'power']['source'])
        power_charge = (
            vitals['diagnostics']['payload']['device'][
                'power']['battery']['charge'])
        power_state = (
            vitals['diagnostics']['payload']['device'][
                'power']['battery']['state'])
        system_uptime = (
            vitals['diagnostics']['payload']['device'][
                'system']['uptime'])
        memory_used = (
            vitals['diagnostics']['payload']['device'][
                'system']['memory']['used'])
        memory_total = (
            vitals['diagnostics']['payload']['device'][
                'system']['memory']['total'])
        memory_pct_used = round(memory_used / float(memory_total) * 100)
        device_status = (
            vitals['diagnostics']['payload']['service'][
                'device']['status'])
        vals = [
            device_id,
            signal_strength, signal_quality,
            connection_status, connection_attempts,
            connection_disconnects, connection_disconnection_reason,
            power_source, power_charge, power_state, system_uptime,
            memory_used, memory_total, memory_pct_used, device_status]
        df = df.append(pd.DataFrame(columns=cols, data=[vals]))
    df.to_csv(
        root + '/data/raw/particle/vitals.csv', index=False)


def last_heard():
    """
    """
    df = pd.read_hdf(
        root + '/data/processed/fulcrum/device_data.h5')
    df = df[['site_id', 'particle_id_outdoor', 'end_date_outdoor']]
    df = df[df['particle_id_outdoor'] != 'LORA']
    site_list = df['site_id'].unique().tolist()
    particle_site_dict = dict()
    for site_id in site_list:
        curr_df = df[df['site_id'] == site_id]
        curr_df.sort_values(
            'end_date_outdoor', inplace=True, ascending=False)
        particle_id = curr_df['particle_id_outdoor'].iloc[0]
        if 'R' in particle_id:
            particle_id = 'ridgeline' + particle_id.split('R')[-1]
        particle_site_dict[particle_id] = site_id
    df = pd.read_csv(root + '/data/raw/particle/devices.csv')
    df = df[['device_name', 'last_heard']]
    df = df[df['device_name'].isin(particle_site_dict.keys())]
    df['site_id'] = df['device_name'].map(particle_site_dict)
    df['last_heard'] = pd.to_datetime(df['last_heard']).dt.date
    df.sort_values('last_heard', inplace=True, ascending=True)
    df.set_index('site_id', inplace=True)
    df.to_csv(
        root + '/data/raw/particle/last_heard.csv')
