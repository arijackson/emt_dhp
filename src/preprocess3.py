# -*- coding: utf-8 -*-
"""
processing data from SD cards
"""
import pandas as pd
import numpy as np
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')


def sd_raw_to_interim():
    """
    """
    dir_path = root + '/data/raw/sd'
    file_list = os.listdir(dir_path)
    i_0 = 0
    i_n = len(file_list)
    i = i_0
    print('')
    for file_name in file_list[i_0:i_n]:
        i += 1
        print(str(i) + ' of ' + str(i_n) + ', ' + file_name.split('.')[0])
        file_path = dir_path + '/' + file_name
        try:
            df_raw = pd.read_json(file_path, lines=True, typ='frame')
        except ValueError:
            print("    Couldn't decode")
            continue
        df_list = list()
        for index, row in df_raw.iterrows():
            curr_df = pd.json_normalize(row['data'])
            curr_id = row['id']
            curr_df['thingspeak_id'] = curr_id.lower()
            curr_df.index = [index]
            df_list.append(curr_df)
        df = pd.concat(df_list, axis=0)
        gateway_thingspeak_id = [
            i for i in
            df['thingspeak_id'].unique().tolist()
            if 'a30b' in i][0]
        df['thingspeak_id'] = np.where(
            ~df['thingspeak_id'].str.contains('a30b'),
            df['thingspeak_id'] + '-' + gateway_thingspeak_id,
            df['thingspeak_id'])
        df['datetime'] = pd.to_datetime(df['t'], unit='s')
        del df['t']
        df.set_index('datetime', inplace=True)
        df.sort_index(inplace=True)
        thingspeak_id_list = df['thingspeak_id'].unique().tolist()
        for thingspeak_id in thingspeak_id_list:
            curr_df = df[df['thingspeak_id'] == thingspeak_id]
            del curr_df['thingspeak_id']
            curr_df.dropna(inplace=True, how='all', axis=1)
            curr_df = curr_df.apply(pd.to_numeric)
            start_datetime = (
                str(curr_df.index[0]).replace(':', '-').replace(' ', '-'))
            end_datetime = (
                str(curr_df.index[-1]).replace(':', '-').replace(' ', '-'))
            file_dst = (
                root + '/data/interim/sd/' +
                thingspeak_id + '_' +
                start_datetime + '_' +
                end_datetime + '_' +
                '.h5')
            curr_df.to_hdf(file_dst, 'df')
