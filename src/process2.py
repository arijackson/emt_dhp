# -*- coding: utf-8 -*-
"""
processing fulcrum data
"""
import pandas as pd
import numpy as np
import datetime as dt
from preprocess2 import get_field_df
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
ts_api_key = os.environ.get('TS_API_KEY')
pd.options.mode.chained_assignment = None


def calc_energy(option, model, ct_amps):
    """
    see wattnode docs, use function to convert puses to energy
    """
    # phases per pulse output
    pppo_dict = {
        'Standard': 3,
        'Option DPO': 3,
        'Option P3': 1,
        'Option PV': 2}			# PV is 2 for P1 and P2, 1 for P3
    pppo = pppo_dict[option]
    # nominal line voltage
    nvac_dict = {
        'WNB-3Y-208-P': 120.,
        'WNB-3Y-400-P': 230.,
        'WNB-3Y-480-P': 277.,
        'WNB-3Y-600-P': 347.,
        'WNB-3D-240-P': 120.,
        'WNB-3D-400-P': 230.,
        'WNB-3D-480-P': 277.}
    nvac = nvac_dict[model]
    # full-scale pulse frequency
    fshz_dict = {
        'WNB-3Y-208-P': 4.,
        'WNB-3Y-400-P': 4.,
        'WNB-3Y-480-P': 4.,
        'WNB-3Y-600-P': 4.,
        'WNB-3D-240-P': 4.,
        'WNB-3D-400-P': 4.,
        'WNB-3D-480-P': 4.}
    fshz = fshz_dict[model]
    # kilowatt hours per pulse
    kwhpp = (pppo * nvac * ct_amps) / (fshz * 3600. * 1000.)
    return kwhpp


def resample_compressor_power(df, ct_amps, freq):
    """
    convert pulses to kW using CT size and frequency in minutes,
    resample to specisified frequency
    """
    df = df.resample(freq).apply(lambda x: x.sum(skipna=True))
    option = 'Standard'
    model = 'WNB-3D-240-P'
    kwhpp = calc_energy(option, model, ct_amps)
    # [pulse/1][kwh/pulse][min/h][min/1]
    df = df * kwhpp * 60.0 / float(freq.split('T')[0])
    # filter out extremlty high pulse counts
    df[df.columns[0]] = np.where(
        df[df.columns[0]] > 50,
        np.nan,
        df[df.columns[0]])
    # filter negative values used as place holder for missing data
    df[df.columns[0]] = np.where(
        df[df.columns[0]] < 0,
        np.nan,
        df[df.columns[0]])
    return df


def resample_fan_current(df, freq):
    """
    convert pulses to current in amps assuming 1A CT and frequency
    in minutes, resample to specisified frequency
    """
    df = df.resample(freq).apply(lambda x: x.sum(skipna=True))
    df = df / float(freq.split('T')[0])
    # updated CT to Pulse conversion
    # 1 pulse = 1 mV, 1 A = 333 mV
    # divided by .69 to correcf for input impedance
    df = df / 333. / 0.69
    # filter out extremlty high pulse counts
    df[df.columns[0]] = np.where(
        df[df.columns[0]] > 50,
        np.nan, df[df.columns[0]])
    # filter negative values used as place holder for missing data
    df[df.columns[0]] = np.where(
        df[df.columns[0]] < 0,
        np.nan,
        df[df.columns[0]])
    return df


def resample_non_pulse(df, freq):
    """
    resample to specisified frequency, use for measurements other than
    pulse counts (temps, voltages, rh, etc)
    """
    df = df.resample(freq).apply(lambda x: x.mean(skipna=False))
    df.fillna(inplace=True, method='ffill', limit=1)
    df.fillna(inplace=True, method='bfill', limit=1)
    return df


def measurement_to_interim(
        outdoor_label, indoor_label,
        file_dst, df_fulcrum, measurement, device_location):
    """
    with the replacement of devices, a measurement of a single quantity
    over time can require requesting data from multiple channels/fields in
    thingspeak, this function iterates over a data frame of devices used to
    take a single measurement and processes and concatenates the data based
    on the type of the measurement
    """
    # index sampled at two minute intervals in Eastern timezone
    freq = '2T'
    timezone = 'America/New_York'
    # instantiate list to concatenate dfs from thingspeak
    df_list = list()
    # check if dataframe already exists
    if os.path.exists(file_dst):
        # if data has already been downloaded then load the existing df
        df_ext = pd.read_hdf(file_dst)
        if not df_ext.empty:
            # set start date for current download to end data of past download
            start_date = df_ext.index[-1].date()
    # iterate over devices used to measure quantity
    for index, row in df_fulcrum.iterrows():
        # parse attributes needed to request data from thingspeak from
        # compiled fulcrum data
        thingspeak_id = row['thingspeak_id_' + device_location]
        measurement_label = row[measurement + '_label']
        end_date = row['end_date_' + device_location]
        if (not os.path.exists(file_dst)) or (df_ext.empty):
            start_date = row['start_date_' + device_location]
        # fan current is an indoor quantity but measured outdoor
        if measurement == 'fan_current':
            thingspeak_id = row['thingspeak_id_outdoor']
            end_date = row['end_date_outdoor']
            if (not os.path.exists(file_dst)) or (df_ext.empty):
                start_date = row['start_date_outdoor']
        # download data for specific device/measurement/period of time, if
        # thingspeak doesn't return any data ("KeyError") then create dummy
        # dataframe to keep track of nulls
        try:
            curr_df = get_field_df(
                thingspeak_id, measurement_label, start_date, end_date)
        except KeyError:
            # because pulses use nans to imply zero, fill dummy df with -1
            if ((measurement_label == 'compressor_power') or
               (measurement_label == 'fan_current')):
                fill_val = -1
            # otherwise use nans for temps/rh/voltages/etc.
            else:
                fill_val = np.nan
            # if a device has not been installed it will have no start date,
            # fill in start date with dummy date set to present minus one day
            if pd.isnull(start_date):
                start_date = dt.datetime.now().date() - dt.timedelta(days=1)
            curr_df = pd.DataFrame(
                fill_val,
                index=pd.date_range(start_date, end_date,
                                    tz=timezone, freq=freq),
                columns=[0])
        # resample/label data based on measurement type
        if measurement == 'compressor_power':
            compressor_ct_size = int(row['compressor_ct_size'])
            curr_df = resample_compressor_power(
                curr_df, compressor_ct_size, freq)
            curr_df.columns = [outdoor_label + '_P']
        elif measurement == 'outdoor_temp':
            curr_df = resample_non_pulse(curr_df, freq)
            curr_df.columns = [outdoor_label + '_OT']
        elif measurement == 'outdoor_battery':
            curr_df = resample_non_pulse(curr_df, freq)
            curr_df.columns = [outdoor_label + '_OB']
        elif measurement == 'fan_current':
            curr_df = resample_fan_current(curr_df, freq)
            curr_df.columns = [outdoor_label + indoor_label + '_C']
        elif measurement == 'supply_temp':
            curr_df = resample_non_pulse(curr_df, freq)
            curr_df.columns = [outdoor_label + indoor_label + '_ST']
        elif measurement == 'supply_rh':
            curr_df = resample_non_pulse(curr_df, freq)
            curr_df.columns = [outdoor_label + indoor_label + '_SH']
        elif measurement == 'return_temp':
            curr_df = resample_non_pulse(curr_df, freq)
            curr_df.columns = [outdoor_label + indoor_label + '_RT']
        elif measurement == 'indoor_battery':
            curr_df = resample_non_pulse(curr_df, freq)
            curr_df.columns = [outdoor_label + indoor_label + '_IB']
        # append data for particular device/point in time to list for
        # concatenation
        df_list.append(curr_df)
    # concat dfs from all devices as continuous measurement of
    # specisified quantity
    df = pd.concat(df_list)
    # combine already downloaded data if present
    if os.path.exists(file_dst):
        df = pd.concat([df_ext, df])
        df.sort_index(inplace=True)
        df = df[~df.index.duplicated(keep='first')]
    # write data to interim
    df.to_hdf(file_dst, 'df')


def site_to_interim(site_id):
    """
    iterate over each measurement take on site and store time series data for
    each quantity
    """
    # filter fulcrum data for site id
    df_fulcrum = pd.read_hdf(root + '/data/processed/fulcrum/device_data.h5')
    df_fulcrum = df_fulcrum[df_fulcrum['site_id'] == site_id]
    # iterate over list of outdoor unit labels
    outdoor_list = df_fulcrum['outdoor_label'].unique().tolist()
    for outdoor_label in outdoor_list:
        df_fulcrum_outdoor = df_fulcrum[
            df_fulcrum['outdoor_label'] == outdoor_label]
        # for each outdoor unit, download data for these measurements
        outdoor_measurement_list = [
            'compressor_power', 'outdoor_temp', 'outdoor_battery']
        for outdoor_measurement in outdoor_measurement_list:
            file_dst = (
                root + '/data/interim/thingspeak/' +
                outdoor_measurement + '/' + site_id + '_' + outdoor_label +
                '.h5')
            # use measurement_to_interim function to store data
            measurement_to_interim(
                outdoor_label, '',
                file_dst, df_fulcrum_outdoor, outdoor_measurement, 'outdoor')
        # iterate over list of indoor units supplied by current outdoor unit
        indoor_list = df_fulcrum_outdoor['indoor_label'].unique().tolist()
        for indoor_label in indoor_list:
            df_fulcrum_indoor = df_fulcrum_outdoor[
                df_fulcrum_outdoor['indoor_label'] == indoor_label]
            # for each indoor unit, download data for these measurements
            indoor_measurement_list = [
                'fan_current', 'supply_temp', 'supply_rh', 'return_temp',
                'indoor_battery']
            for indoor_measurement in indoor_measurement_list:
                file_dst = (
                    root + '/data/interim/thingspeak/' +
                    indoor_measurement + '/' +
                    site_id + '_' + outdoor_label + indoor_label +
                    '.h5')
                # use measurement_to_interim function to store data
                measurement_to_interim(
                    outdoor_label, indoor_label,
                    file_dst, df_fulcrum_indoor, indoor_measurement, 'indoor')


def site_to_processed(site_id):
    """
    combine all measurements take on site into single dataframe,
    conduct some post processing of pulse data that relies on having more
    than one measurement taken concurrently, add columns for quantities
    that should have been measured but aren't appearing in the data
    """
    # merge all available data for site
    site_df = pd.DataFrame()
    measurement_list = [
        'compressor_power', 'fan_current', 'outdoor_temp', 'return_temp',
        'supply_temp', 'supply_rh', 'outdoor_battery', 'indoor_battery']
    for measurement in measurement_list:
        dir_path = root + '/data/interim/thingspeak/' + measurement
        file_list = os.listdir(dir_path)
        file_list = [i for i in file_list if site_id == i.split('_')[0]]
        for file_name in file_list:
            df = pd.read_hdf(dir_path + '/' + file_name)
            site_df = pd.merge(
                site_df, df, how='outer', left_index=True, right_index=True)
    # filter pulses using battery/outdoor temp, pulse counts not reporting
    # zeros creates an issue because we don't know if a missing value is
    # zero or actually missing, so here we compare against other measurements
    # taken from same device that will always record unless device is offline
    for col in site_df.columns:
        # for power measurements, compare data against fan current,
        # outdoor temp, and outdoor battery
        if 'P' in col:
            outdoor_label = col[0]
            comps = [
                i for i in site_df.columns if
                (outdoor_label in i) &
                (('_C' in i) |
                 ('_OT' in i) |
                 ('_OB' in i))]
            sub_cols = comps.copy()
            sub_cols.append(col)
            sub_df = site_df[sub_cols]
            for sub_col in sub_df.columns:
                if '_C' in sub_col:
                    sub_df[sub_col] = np.where(
                        sub_df[sub_col] <= 0, np.nan, sub_df[sub_col])
            mask = sub_df[comps].notnull().max(axis=1)
            site_df[col] = np.where(
                ~mask,
                np.nan, site_df[col])
        # for current measurements, compare data against power,
        # outdoor temp, and outdoor battery
        if 'C' in col:
            outdoor_label = col[0]
            comps = [
                i for i in site_df.columns if
                (outdoor_label in i) &
                (('_P' in i) |
                 ('_OT' in i) |
                 ('_OB' in i))]
            sub_cols = comps.copy()
            sub_cols.append(col)
            sub_df = site_df[sub_cols]
            for sub_col in sub_df.columns:
                if '_P' in sub_col:
                    sub_df[sub_col] = np.where(
                        sub_df[sub_col] <= 0, np.nan, sub_df[sub_col])
            mask = sub_df[comps].notnull().max(axis=1)
            site_df[col] = np.where(
                ~mask,
                np.nan, site_df[col])
    # add missing columns where data is expected but is not currently
    # showing up
    indoor_unit_list = list(set(
        [i.split('_')[0] for i in site_df.columns
         if len(i.split('_')[0]) == 2]))
    for indoor_label in indoor_unit_list:
        outdoor_label = indoor_label[0]
        column_list = [
            outdoor_label + '_P', outdoor_label + '_OT', outdoor_label + '_OB',
            indoor_label + '_C', indoor_label + '_RT',
            indoor_label + '_ST', indoor_label + '_SH', indoor_label + '_IB']
        for column in column_list:
            if column not in site_df.columns:
                site_df[column] = np.nan
    site_df.to_hdf(
        root + '/data/processed/thingspeak/' + site_id + '.h5', 'df')
    site_df.to_csv(
        root + '/data/processed/thingspeak/' + site_id + '.csv')
