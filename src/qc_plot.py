# -*- coding: utf-8 -*-
"""
generating plots for data qc
"""
import pandas as pd
import datetime as dt
import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from PyPDF2 import PdfFileMerger
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')


def site_qc(site_id, show):
    """
    plot time series data for all indoor and outdoor devices used at a site
    """
    fig_list = list()
    df = pd.read_hdf(root + '/data/processed/thingspeak/' + site_id + '.h5')
    df_fulcrum = pd.read_hdf(root + '/data/processed/fulcrum/device_data.h5')
    df_fulcrum = df_fulcrum.groupby('site_id').agg(
        {'start_date_outdoor': 'min', 'end_date_outdoor': 'max'})
    install_date = df_fulcrum.loc[site_id]['start_date_outdoor']
    download_date = (
        df_fulcrum.loc[site_id]['end_date_outdoor'] + dt.timedelta(days=1))
    # create one plot for each indoor unit and supplying outdoor unit
    indoor_label_list = [i.split('_')[0] for i in df.columns if '_C' in i]
    for indoor_label in indoor_label_list:
        outdoor_label = indoor_label[0]
        plt.close('all')
        fig, ax_list = plt.subplots(nrows=5, sharex=True, figsize=(12, 6))
        # get time series
        compressor_power = df[outdoor_label + '_P']
        fan_current = df[indoor_label + '_C']
        outdoor_temp = df[outdoor_label + '_OT']
        return_temp = df[indoor_label + '_RT']
        supply_temp = df[indoor_label + '_ST']
        supply_rh = df[indoor_label + '_SH']
        outdoor_battery = df[outdoor_label + '_OB']
        indoor_battery = df[indoor_label + '_IB']
        # plot download/install dates for each axes, add title and xlabel
        for ax in ax_list:
            ax.axvline(
                install_date,
                color='k', lw=1, ls='--', label=None)
            ax.axvline(
                download_date,
                color='k', lw=1, ls='--', label=None)
            if ax is ax_list[0]:
                ax.set_title(site_id + ' ' + indoor_label, fontsize=10)
            if ax is ax_list[-1]:
                fmt_week = mdates.DayLocator(interval=7)
                ax.xaxis.set_major_locator(fmt_week)
                fmt_day = mdates.DayLocator()
                ax.xaxis.set_minor_locator(fmt_day)
                ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d'))

        # helper function for formatting
        def plot_measurement(ax, df, legend_label, y_label, color):
            ax.plot(
                df.index.to_series().apply(lambda x: x.replace(tzinfo=None)),
                df.values, label=legend_label,
                color=color, lw=1, rasterized=True)
            ax.set_ylabel(y_label, fontsize=6)
            ax.tick_params(labelsize=6)
            ax.set_xlabel(None)
            leg = ax.legend(loc='upper left', fontsize=6, framealpha=1)
            frame = leg.get_frame()
            frame.set_facecolor('white')
            frame.set_edgecolor('black')

        # plot each measurement
        plot_measurement(
            ax_list[0], compressor_power, 'Compressor', 'Power [kW]', 'k')
        plot_measurement(
            ax_list[1], fan_current, 'Fan', 'Current [A]', 'k')
        plot_measurement(
            ax_list[2], outdoor_temp, 'Outdoor', 'Temperature [F]', 'b')
        plot_measurement(
            ax_list[2], supply_temp, 'Supply', 'Temperature [F]', 'k')
        plot_measurement(
            ax_list[2], return_temp, 'Return', 'Temperature [F]', 'g')
        plot_measurement(
            ax_list[3], supply_rh, 'Supply', 'Relative Humidigy [%]', 'k')
        plot_measurement(
            ax_list[4], outdoor_battery, 'Outdoor Battery', 'Voltage [V]', 'k')
        plot_measurement(
            ax_list[4], indoor_battery, 'Indoor Battery', 'Voltage [V]', 'g')
        # maximize/tighten frame, display if show is true otherwise save pdf
        plt.tight_layout()
        if show is True:
            plt.show()
        else:
            fig_list.extend(
                [manager.canvas.figure
                 for manager in
                 matplotlib._pylab_helpers.Gcf.get_all_fig_managers()])
            plt.close('all')
    # combine plots of indoor units to write pdf for each site
    if show is not True:
        pdf_path = (
            root + '/data/processed/plots/sites/' +
            site_id + '.pdf')
        pdf = PdfPages(pdf_path)
        for fig in fig_list:
            pdf.savefig(fig)
        pdf.close()


def merge_site_pdfs():
    """
    merge all site pdfs into one
    """
    dir_path = root + '/data/processed/plots/sites'
    pdfs = os.listdir(dir_path)
    df = pd.DataFrame()
    df['file_name'] = pdfs
    df['site_id'] = df['file_name'].str.split('.').str[0].apply(int)
    df.sort_values(by='site_id', ascending=True, inplace=True)
    pdfs = df['file_name'].values.tolist()
    merger = PdfFileMerger()
    for pdf in pdfs:
        merger.append(dir_path + '/' + pdf)
    merger.write(root + '/data/processed/plots/site_qc.pdf')
    merger.close()


if __name__ == '__main__':
    merge_site_pdfs()
