# -*- coding: utf-8 -*-
"""
fulcrum data raw -> interim

unzipping export files and minor processing/parsing/relabeling
"""
import pandas as pd
import datetime as dt
import platform
import warnings
import os
import zipfile
import shutil
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
pd.options.mode.chained_assignment = None  # default='warn'
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


def unzip_fulcrum_export():
    """
    unzip fulcrum export from downloads and move to project directory
    """
    # determine if running on Mac or Windows, then get path to downloads
    system = platform.system()
    if system == 'Windows':
        path_to_downloads = os.path.join(
            os.getenv('USERPROFILE'), 'Downloads').replace('\\', '/').lower()
    elif system == 'Darwin':		# Mac is Darwin
        path_to_downloads = (
            '/Users/' + os.environ.get('USER') + '/Downloads').lower()
    # list Fulcrum exports
    file_list = os.listdir(path_to_downloads)
    fulcrum_file_list = [
        i for i in file_list if ('Fulcrum_Export' in i) and ('.zip' in i)]
    # print reminder if no new fulcrum data has been downloaded
    if len(fulcrum_file_list) == 0:
        print('    No new Fulcrum export has been downloaded.')
    # otherwise extract files from zip folder
    else:
        # get download date for each fulcrum export and check that export
        # is for EMT fulcrum app
        file_date_list = list()
        fulcrum_emt_file_list = list()
        for file_name in fulcrum_file_list:
            zip_file = zipfile.ZipFile(path_to_downloads + '/' + file_name)
            if 'emt/' not in zip_file.namelist():
                continue
            file_date = dt.datetime.fromtimestamp(
                float(os.path.getatime(
                    path_to_downloads + '/' + file_name)))
            file_date_list.append((file_name, file_date))
            fulcrum_emt_file_list.append(file_name)
        if len(file_date_list) == 0:
            print('')
            print('    No new Fulcrum export has been downloaded.')
            return
        # sort files by download date
        file_date_list_sorted = sorted(
            file_date_list, key=lambda tup: tup[1], reverse=True)
        # get most recenlty downloaded Fulcrum export
        zip_file = file_date_list_sorted[0][0]
        # extract and move unzipped folder to data/raw/fulcrum
        path_to_zip_file = path_to_downloads + '/' + zip_file
        directory_to_extract_to = root + '/data/raw/fulcrum'
        with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
            zip_ref.extractall(directory_to_extract_to)
        # move files from 'emt' folder to data/raw/fulcrum
        for file_name in os.listdir(root + '/data/raw/fulcrum/emt'):
            if '.jpg' not in file_name:
                shutil.move(
                    root + '/data/raw/fulcrum/emt/' + file_name,
                    root + '/data/raw/fulcrum/' + file_name)
            else:
                shutil.move(
                    root + '/data/raw/fulcrum/emt/' + file_name,
                    root + '/data/raw/fulcrum/images/' + file_name)
        # delete fulcrum exports in downloads once most recent download
        # has been extracted
        for file_name in fulcrum_emt_file_list:
            os.remove(path_to_downloads + '/' + file_name)


def process_emt():
    """
    site attribute data processing
    """
    # load export file
    df = pd.read_csv(
        root + '/data/raw/fulcrum/emt.csv', dtype=str)
    # identify columns to keep for analysis
    to_keep = [
        'fulcrum_id', 'site_id', 'homeowner_name', 'town', 'address']
    df = df[to_keep]
    # rename identifiers for later merging
    df.rename(columns={'fulcrum_id': 'id0'}, inplace=True)
    # write data to interim
    df.to_hdf(root + '/data/interim/fulcrum/emt.h5', 'df')
    df.to_csv(root + '/data/interim/fulcrum/emt.csv', index=False)


def process_emt_cellular_node():
    """
    cellular node attribute data processing
    """
    # load data
    df = pd.read_csv(
        root + '/data/raw/fulcrum/emt_cellular_node.csv', dtype=str)
    # delete unnecessary columns
    to_keep = [
        'fulcrum_id', 'fulcrum_parent_id', 'particle_id', 'thingspeak_id',
        'particle_id_other', 'thingspeak_id_other', 'meter_location_type',
        'room_type', 'supplying_heat_pump_outdoor_label',
        'heat_pump_indoor_label', 'cellular_node_channel_label_return_temp',
        'cellular_node_channel_label_supply_temp', 'heat_pump_indoor_model',
        'central_heating_system_label', 'central_heating_system_type',
        'central_heating_system_type_other',
        'central_heating_system_supply_temp',
        'central_heating_system_hot_water_temp',
        'central_heating_system_ambient_temp',
        'replacement_status', 'install_date']
    df = df[to_keep]
    # rename identifiers for later merging
    df.rename(
        columns={
            'fulcrum_parent_id': 'id0',
            'fulcrum_id': 'id1'},
        inplace=True)
    # cleaning up columns based on labeling conventions from the field
    # data collection
    df['particle_id'] = (
        df['particle_id'].fillna('') +
        df['particle_id_other'].fillna(''))
    del df['particle_id_other']
    df['particle_id'] = df['particle_id'].str.upper()
    df['particle_id'] = df['particle_id'].apply(
        lambda x: x.replace('R', 'ridgeline') if 'LO' not in x else x)
    df['thingspeak_id'] = (
        df['thingspeak_id'].fillna('') +
        df['thingspeak_id_other'].fillna(''))
    del df['thingspeak_id_other']
    df['thingspeak_id'] = df['thingspeak_id'].apply(
        lambda x: x.split('-')[0] if len(x.split('-')) <= 2 else x)
    df['thingspeak_id'] = df['thingspeak_id'].apply(
        lambda x: '-'.join(x.split('-')[:-1]) if len(x.split('-')) == 3 else x)
    df['thingspeak_id'] = df['thingspeak_id'].str.lower()
    df['central_heating_system_type'] = (
        df['central_heating_system_type'].fillna('') +
        df['central_heating_system_type_other'].fillna(''))
    del df['central_heating_system_type_other']
    # split dataframes into outdoor, indoor, and central heating, for
    # each dataframe select only necessary columns
    df_outdoor = df[df['meter_location_type'] == 'Outdoor Heat Pump Unit']
    cols = ['id1', 'id0', 'particle_id', 'thingspeak_id', 'replacement_status',
            'install_date']
    df_outdoor = df_outdoor[cols]
    df_indoor = df[df['meter_location_type'] == 'Indoor Heat Pump Unit']
    df_indoor['id4'] = (
        df_indoor['id0'] + '-' +
        df_indoor['supplying_heat_pump_outdoor_label'] + '-' +
        df_indoor['heat_pump_indoor_label'])
    cols = ['id1', 'id0', 'id4', 'particle_id', 'thingspeak_id', 'room_type',
            'supplying_heat_pump_outdoor_label', 'heat_pump_indoor_label',
            'cellular_node_channel_label_return_temp',
            'cellular_node_channel_label_supply_temp',
            'heat_pump_indoor_model', 'replacement_status', 'install_date']
    df_indoor = df_indoor[cols]
    df_heating = df[df['meter_location_type'] == 'Central Heating System']
    cols = ['id1', 'id0', 'particle_id', 'thingspeak_id',
            'central_heating_system_label', 'central_heating_system_type',
            'central_heating_system_supply_temp',
            'central_heating_system_hot_water_temp',
            'central_heating_system_ambient_temp', 'replacement_status',
            'install_date']
    df_heating = df_heating[cols]
    # writing data
    df_outdoor.to_hdf(
        root + '/data/interim/fulcrum/emt_cellular_node_outdoor.h5', 'df')
    df_outdoor.to_csv(
        root + '/data/interim/fulcrum/emt_cellular_node_outdoor.csv',
        index=False)
    df_indoor.to_hdf(
        root + '/data/interim/fulcrum/emt_cellular_node_indoor.h5', 'df')
    df_indoor.to_csv(
        root + '/data/interim/fulcrum/emt_cellular_node_indoor.csv',
        index=False)
    df_heating.to_hdf(
        root + '/data/interim/fulcrum/emt_cellular_node_heating.h5', 'df')
    df_heating.to_csv(
        root + '/data/interim/fulcrum/emt_cellular_node_heating.csv',
        index=False)


def process_emt_heat_pump_outdoor():
    """
    outdoor heat pump attribute data processing
    """
    # load data and identify columns to keep
    df = pd.read_csv(
        root + '/data/raw/fulcrum/emt_cellular_node_heat_pump_outdoor.csv',
        dtype=str)
    to_keep = [
        'fulcrum_id', 'fulcrum_parent_id', 'fulcrum_record_id',
        'heat_pump_outdoor_label', 'heat_pump_outdoor_model',
        'cellular_node_channel_label_power', 'wattnode_voltage',
        'ct_size_power', 'ct_size_power_other',
        'cellular_node_channel_label_temperature']
    df = df[to_keep]
    # relabel identifiers for merging datasets
    df.rename(
        columns={
            'fulcrum_parent_id': 'id1',
            'fulcrum_record_id': 'id0',
            'fulcrum_id': 'id2'},
        inplace=True)
    # minor cleaning of columns
    df['ct_size_power'] = (
        df['ct_size_power'].fillna('') +
        df['ct_size_power_other'].fillna(''))
    del df['ct_size_power_other']
    # write data to interim
    df.to_hdf(
        root + '/data/interim/fulcrum/emt_heat_pump_outdoor.h5', 'df')
    df.to_csv(
        root + '/data/interim/fulcrum/emt_heat_pump_outdoor.csv',
        index=False)


def process_emt_heat_pump_indoor_current():
    """
    heat pump indoor attribute data processing
    """
    # load data and select columns to keep for analysis
    df = pd.read_csv(
        root + '/data/raw/fulcrum/' +
        'emt_cellular_node_heat_pump_outdoor_heat_pump_indoor_current.csv',
        dtype=str)
    to_keep = [
        'fulcrum_id', 'fulcrum_parent_id', 'fulcrum_record_id',
        'heat_pump_indoor_label_current', 'ct_size_current',
        'ct_size_current_other', 'cellular_node_channel_label_current']
    df = df[to_keep]
    # relabel identifiers
    df.rename(
        columns={
            'fulcrum_parent_id': 'id2',
            'fulcrum_record_id': 'id0',
            'fulcrum_id': 'id3'},
        inplace=True)
    # minor cleaning
    df['ct_size_current'] = (
        df['ct_size_current'].fillna('') +
        df['ct_size_current_other'].fillna(''))
    del df['ct_size_current_other']
    # write data to interim
    df.to_hdf(
        root + '/data/interim/fulcrum/emt_heat_pump_indoor_current.h5', 'df')
    df.to_csv(
        root + '/data/interim/fulcrum/emt_heat_pump_indoor_current.csv',
        index=False)


if __name__ == '__main__':
    unzip_fulcrum_export()
    process_emt()
    process_emt_cellular_node()
    process_emt_heat_pump_outdoor()
    process_emt_heat_pump_indoor_current()
