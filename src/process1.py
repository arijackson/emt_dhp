# -*- coding: utf-8 -*-
"""
processing fulcrum data

merging data needed for analysis into single flat file, geocoding addressed
and merging enrollment numbers with fulcrum data
"""
import pandas as pd
import numpy as np
import datetime as dt
from geopy import geocoders
import time
import warnings
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
bing_api_key = os.environ.get('BINGAPIKEY')
pd.options.mode.chained_assignment = None  # default='warn'
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


def fulcrum_to_processed():
    """
    compiling all attributes collected in fulcrum needed to download, process,
    and merge thingspeak data for each device and measurement
    """
    # Create df with index id0 through id4 that appear in fulcrum files,
    # each row to represent attributes of an indoor unit and its
    # supplying outdoor unit
    df1 = pd.read_hdf(
        root + '/data/interim/fulcrum/emt_heat_pump_indoor_current.h5')
    df2 = pd.read_hdf(
        root + '/data/interim/fulcrum/emt_cellular_node_indoor.h5')
    df3 = pd.read_hdf(
        root + '/data/interim/fulcrum/emt_heat_pump_outdoor.h5')
    df4 = pd.read_hdf(
        root + '/data/interim/fulcrum/emt_cellular_node_outdoor.h5')
    df5 = pd.read_hdf(
        root + '/data/interim/fulcrum/emt.h5')
    # use id0, id2, and id3 fom df1
    df = df1[['id3', 'id0', 'id2']]
    # add id1 from df3
    df['id1'] = df['id2'].map(
        df3.set_index('id2')['id1'].to_dict())
    # create id4 using combo of indoor label and outdoor label
    df['heat_pump_indoor_label_current'] = df['id3'].map(
        df1.set_index('id3')['heat_pump_indoor_label_current'].to_dict())
    df['heat_pump_outdoor_label'] = df['id2'].map(
        df3.set_index('id2')['heat_pump_outdoor_label'].to_dict())
    df['id4'] = (
        df['id0'] + '-' +
        df['heat_pump_outdoor_label'] + '-' +
        df['heat_pump_indoor_label_current'])
    # clean up and order columns
    del df['heat_pump_outdoor_label'], df['heat_pump_indoor_label_current']
    df = df[['id0', 'id1', 'id2', 'id3', 'id4']]
    # set indicies from fulcrum files to map attributes to df
    df1.set_index('id3', inplace=True)
    df2.set_index('id4', inplace=True)
    df3.set_index('id2', inplace=True)
    df4.set_index('id1', inplace=True)
    df5.set_index('id0', inplace=True)
    # map fields needed for analysis from each of the five fulcrum files
    df['site_id'] = df['id0'].map(df5['site_id'].to_dict())
    df['particle_id_outdoor'] = df['id1'].map(df4['particle_id'].to_dict())
    df['particle_id_indoor'] = df['id4'].map(df2['particle_id'].to_dict())
    df['thingspeak_id_outdoor'] = df['id1'].map(df4['thingspeak_id'].to_dict())
    df['thingspeak_id_indoor'] = df['id4'].map(df2['thingspeak_id'].to_dict())
    df['outdoor_label'] = df['id2'].map(
        df3['heat_pump_outdoor_label'].to_dict())
    df['indoor_label_1'] = df['id4'].map(
        df2['heat_pump_indoor_label'].to_dict())
    df['indoor_label_2'] = df['id3'].map(
        df1['heat_pump_indoor_label_current'].to_dict())
    df['indoor_label'] = df['indoor_label_1']
    df['indoor_label'] = np.where(
        pd.isnull(df['indoor_label']),
        df['indoor_label_2'], df['indoor_label'])
    del df['indoor_label_1'], df['indoor_label_2']
    df['compressor_power_label'] = df['id2'].map(
        df3['cellular_node_channel_label_power'].to_dict())
    df['outdoor_temp_label'] = df['id2'].map(
        df3['cellular_node_channel_label_temperature'].to_dict())
    df['fan_current_label'] = df['id3'].map(
        df1['cellular_node_channel_label_current'].to_dict())
    df['supply_temp_label'] = df['id4'].map(
        df2['cellular_node_channel_label_supply_temp'].to_dict())
    df['supply_rh_label'] = 'RH'
    df['return_temp_label'] = df['id4'].map(
        df2['cellular_node_channel_label_return_temp'].to_dict())
    df['outdoor_battery_label'] = 'V'
    df['indoor_battery_label'] = 'V'
    df['compressor_ct_size'] = df['id2'].map(df3['ct_size_power'].to_dict())
    # device start dates assumed to be when fulcrum record was created,
    # end dates are initially assumed to be the present and updated below
    # for when a device was replaced
    df['start_date_indoor'] = pd.to_datetime(
        df['id4'].map(df2['install_date'].to_dict())).dt.date
    df['end_date_indoor'] = dt.datetime.now().date()
    df['start_date_outdoor'] = pd.to_datetime(
        df['id1'].map(df4['install_date'].to_dict())).dt.date
    df['end_date_outdoor'] = dt.datetime.now().date()
    # removing indicies after all attributes have been mapped
    del df['id0'], df['id1'], df['id2'], df['id3'], df['id4']
    # correct end dates for devices that have been replaced
    df.sort_values(
        ['site_id',
         'outdoor_label', 'indoor_label',
         'start_date_outdoor', 'start_date_indoor'],
        inplace=True, ascending=True)
    for index, row in df.iterrows():
        site_id = row['site_id']
        outdoor_label = row['outdoor_label']
        indoor_label = row['indoor_label']
        # if more than one row exists with same site id and outdoor/indoor
        # label then a device was replaced
        curr_df1 = df[
            (df['site_id'] == site_id) &
            (df['outdoor_label'] == outdoor_label) &
            (df['indoor_label'] == indoor_label)]
        if len(curr_df1) > 1:
            # holding thingspeak_id_indoor constant, update outdoor_end_date
            thingspeak_id_indoor_list = curr_df1[
                'thingspeak_id_indoor'].unique().tolist()
            for thingspeak_id_indoor in thingspeak_id_indoor_list:
                curr_df2 = curr_df1[
                    curr_df1['thingspeak_id_indoor'] == thingspeak_id_indoor]
                if index in curr_df2.index:
                    curr_df2['end_date_outdoor'] = curr_df2[
                        'start_date_outdoor'].shift(-1)
                    curr_df2['end_date_outdoor'].iloc[-1] = (
                        dt.datetime.now().date())
                    df.at[index, 'end_date_outdoor'] = (
                        curr_df2.loc[index]['end_date_outdoor'])
            # holding thingspeak_id_outdoor constant, update indoor_end_date
            thingspeak_id_outdoor_list = curr_df1[
                'thingspeak_id_outdoor'].unique().tolist()
            for thingspeak_id_outdoor in thingspeak_id_outdoor_list:
                curr_df2 = curr_df1[
                    curr_df1['thingspeak_id_outdoor'] == thingspeak_id_outdoor]
                if index in curr_df2.index:
                    curr_df2['end_date_indoor'] = curr_df2[
                        'start_date_indoor'].shift(-1)
                    curr_df2['end_date_indoor'].iloc[-1] = (
                        dt.datetime.now().date())
                    df.at[index, 'end_date_indoor'] = (
                        curr_df2.loc[index]['end_date_indoor'])
    # update indoor unit thingspeak ids to include gateway ids
    for index, row in df.iterrows():
        site_id = row['site_id']
        outdoor_label = row['outdoor_label']
        indoor_label = row['indoor_label']
        thingspeak_id_outdoor = row['thingspeak_id_outdoor']
        thingspeak_id_indoor = row['thingspeak_id_indoor']
        particle_id_outdoor = row['particle_id_outdoor']
        particle_id_indoor = row['particle_id_indoor']
        start_date_outdoor = row['start_date_outdoor']
        start_date_indoor = row['start_date_indoor']
        end_date_outdoor = row['end_date_outdoor']
        end_date_indoor = row['end_date_indoor']
        # special case for site 62: the gateway for site 62 is at site 61,
        # which is close by, for that reason, the gateway id has
        # already been appended to the lora id in fulcrum
        if site_id == '62':
            continue
        # for each row, figure out what gateway was inplace at that time
        # update outdoor id
        curr_df = df[df['site_id'] == site_id]
        thingspeak_id_gateway_list = curr_df[
            curr_df['particle_id_outdoor'].str.contains('G')][
            'thingspeak_id_outdoor'].unique().tolist()
        for thingspeak_id_gateway in thingspeak_id_gateway_list:
            start_date_gateway = curr_df[
                curr_df['thingspeak_id_outdoor'] == thingspeak_id_gateway][
                    'start_date_outdoor'].iloc[0]
            end_date_gateway = curr_df[
                curr_df['thingspeak_id_outdoor'] == thingspeak_id_gateway][
                    'end_date_outdoor'].iloc[0]
            # check if lora and that lora and gateway overlapped
            if (
                ('LORA' in str(particle_id_outdoor)) &
                ((end_date_outdoor > start_date_gateway) or
                 (start_date_outdoor < end_date_gateway))):
                updated_thingspeak_id_outdoor = (
                    thingspeak_id_outdoor + '-' + thingspeak_id_gateway)
                df.at[index, 'thingspeak_id_outdoor'] = (
                    updated_thingspeak_id_outdoor)
            if (
                ('LORA' in str(particle_id_indoor)) &
                ((end_date_indoor > start_date_gateway) or
                 (start_date_indoor < end_date_gateway)) &
                ((thingspeak_id_outdoor == thingspeak_id_gateway) or
                 ('LORA' == particle_id_outdoor) or
                 ('ridgeline' in particle_id_outdoor))):
                updated_thingspeak_id_indoor = (
                    thingspeak_id_indoor + '-' + thingspeak_id_gateway)
                df.at[index, 'thingspeak_id_indoor'] = (
                    updated_thingspeak_id_indoor)
    #TODO: address edge cases # noqa
    # site 40 needs attention (if ridgline or LoRA outdoor device occurs
    # with replaced gateway then additional row is needed for corresponding
    # indoor unit), also 63, 68, 69, 85, 86,
    #TODO:  some duplicates (110) not sure why # noqa
    #TODO: look into LORAs recording before duplicates issue # noqa
    #TODO: adding heating system supply temp # noqa
    # sort by site_id
    df['site_id'] = df['site_id'].apply(int)
    df.sort_values('site_id', inplace=True)
    df['site_id'] = df['site_id'].apply(str)
    # write to csv/h5
    df.to_hdf(
        root + '/data/processed/fulcrum/device_data.h5', 'df')
    df.to_csv(
        root + '/data/processed/fulcrum/device_data.csv', index=False)


def geocode_addresses():
    """
    Get Lat/Lon of scheduled sites in study, coords used for mapping
    """
    # instantiate dataframe to populate
    df = pd.read_csv(root + '/data/raw/project/sites.csv', dtype=str)
    df['address_full'] = (
        df['address'] + ', ' + df['city'] + ', ' + df['state'])
    df.set_index('address_full', inplace=True, drop=False)
    df['county'] = 'na'
    df['lat'] = 'na'
    df['lon'] = 'na'
    # map previously geocoded addresses
    df_coords = pd.read_csv(
        root + '/data/raw/project/site_coords.csv', dtype=str)
    df_coords.set_index('address_full', inplace=True, drop=False)
    df['county'] = df.index.map(df_coords['county'].to_dict())
    df['lat'] = df.index.map(df_coords['lat'].to_dict())
    df['lon'] = df.index.map(df_coords['lon'].to_dict())
    # iterate over df and geocode remaining addresses
    df.fillna('na', inplace=True)
    g = geocoders.Bing(api_key=bing_api_key)
    for i in df.index:
        full_address = i
        if df[df.index == full_address]['lat'].iloc[0] == 'na':
            time.sleep(1)
            try:
                loc = g.geocode(full_address)
                lat = loc.latitude
                lon = loc.longitude
                county = loc.raw['address']['adminDistrict2'].split(' ')[0]
                df.at[i, 'county'] = county
                df.at[i, 'lat'] = lat
                df.at[i, 'lon'] = lon
            except:  # noqa, GeocoderQuotaExceeded
                print("couldn't geocode " + i)
                continue
    df.to_csv(root + '/data/raw/project/site_coords.csv', index=False)
    df.to_csv(root + '/data/interim/project/site_coords.csv', index=False)


def merge_enrollment_numbers():
    """
    merge EMT enrollment numbers with fulcrum data
    """
    df = pd.read_excel(
        root + '/data/raw/project/EM_SitesVisited_211019.xlsx',
        dtype=str)
    df.dropna(inplace=True, axis=1, how='all')
    df['address'] = (
        df['Address'].str.strip().str.lower().str.replace(
            '  ', ' ', regex=True).str.replace('.', '', regex=True) +
        ', ' +
        df['City'].str.strip().str.lower().str.replace(
            '  ', ' ', regex=True))
    df.set_index('address', inplace=True)
    address_enrollment_dict = df['Enrollment Number'].to_dict()
    df = pd.read_csv(
        root + '/data/interim/fulcrum/emt.csv', dtype=str)
    df['address 2'] = (
        df['address'].str.strip().str.lower().str.replace(
            '  ', ' ', regex=True).str.replace('.', '', regex=True) +
        ', ' +
        df['town'].str.strip().str.lower().str.replace(
            '  ', ' ', regex=True))
    site_enrollment_dict = dict()
    for index, row in df.iterrows():
        site_id = row['site_id']
        address = row['address 2']
        enrollment_number = address_enrollment_dict[address]
        site_enrollment_dict[site_id] = enrollment_number
    del df['address 2']
    df['enrollment_number'] = df['site_id'].map(site_enrollment_dict)
    df.to_csv(root + '/data/interim/fulcrum/emt.csv', index=False)
    df.to_hdf(root + '/data/interim/fulcrum/emt.h5', 'df')


if __name__ == '__main__':
    fulcrum_to_processed()
    geocode_addresses()
    merge_enrollment_numbers()
