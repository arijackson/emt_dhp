# -*- coding: utf-8 -*-
"""
"""
import pandas as pd
import process2
import time
import qc_plot
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')


def run():
    site_id = input(
        "Enter the site ID you'd like to process. "
        "To process all sites, type all: ")
    if site_id == 'all':
        df_fulcrum = pd.read_hdf(
            root + '/data/processed/fulcrum/device_data.h5')
        site_list = df_fulcrum['site_id'].unique().tolist()
        i_0 = 0
        i_n = len(site_list)
        i = i_0
        print('')
        print('Processing Data')
        for site_id in site_list[i_0:i_n]:
            i += 1
            print('')
            print(str(i) + ' of ' + str(i_n) + ', ' + site_id)
            try:
                process2.site_to_interim(site_id)
                process2.site_to_processed(site_id)
            except:				# noqa
                j = 0
                while j < 3:
                    time.sleep(60)
                    j += 1
                    try:
                        process2.site_to_interim(site_id)
                        process2.site_to_processed(site_id)
                    except:		# noqa
                        continue
                continue
        i_0 = 0
        i_n = len(site_list)
        i = i_0
        print('')
        print('Plotting Data')
        for site_id in site_list[i_0:i_n]:
            i += 1
            print('')
            print(str(i) + ' of ' + str(i_n) + ', ' + site_id)
            try:
                qc_plot.site_qc(site_id, show=False)
            except:				# noqa
                continue
        qc_plot.merge_site_pdfs()
    else:
        process2.site_to_interim(site_id)
        process2.site_to_processed(site_id)
        show = input(
            "Would you like to see the plot? (yes/no): ")
        if show.lower() == 'yes':
            qc_plot.site_qc(site_id, show=True)
        else:
            qc_plot.site_qc(site_id, show=False)


if __name__ == '__main__':
    run()
