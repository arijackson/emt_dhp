# -*- coding: utf-8 -*-
"""
Script to write .env file from current working directory, run using
command [$ make dotenv]

Compiling from /src directory will set /src as root
"""
import os

# Makefile run from project directory so cwd isn't /src
dir_path = os.getcwd().replace('\\', '/')
# assign cwd to root
root = "ROOT = '" + dir_path + "'"
# check if .env file already exists so not to overwrite
# other manually entered variables
# (like database credentials)
if not os.path.exists(dir_path + '/.env'):
    cmd = 'echo ' + root + ' > .env'
    os.system(cmd)
# if .env exists then look for ROOT variable and rewrite
# while maintining rest of file
else:
    # read current .env file
    with open(dir_path + '/.env') as f:
        content = f.readlines()
    # remove new line, will be added back in write
    content = [i.replace('\n', '') for i in content]
    # look for root and record index to reset
    root_index = ''
    for i in range(len(content[0:])):
        line = content[i]
        if 'ROOT' in line:
            root_index = i
    # if root was found then re-assing variable
    if root_index != '':
        content[root_index] = root
    # else append root to content
    else:
        content.append(root)
    # write contents of file back to .env
    with open(dir_path + '/.env', 'w') as f:
        for line in content:
            # don't write empty lines
            if line != '':
                f.write("%s\n" % line)
